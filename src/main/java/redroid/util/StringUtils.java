package redroid.util;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import java.io.StringReader;
import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * 字符串帮助类
 * @author RobinVanYang created at 2017-6-19.
 */

public class StringUtils {
    /**
     * MD5消息摘要算法
     * @param message  the input string.
     * @return digest.
     */
    public static String md5(String message) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(message.getBytes());
            return new BigInteger(1, md.digest()).toString(16);
        } catch (Exception e) {
        }
        return "";
    }

    public static boolean notNull(String str) {
        return null != str && str.trim().length() != 0 && (!"null".equals(str.trim()));
    }

    /**
     * if the string is json format, this method will format it.
     * @param string
     * @return
     */
    public static String toPrettyJson(String string) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser parser = new JsonParser();
        JsonReader reader = new JsonReader(new StringReader(string));
        reader.setLenient(true);
        JsonElement element = parser.parse(reader);
        if (element.isJsonObject() || element.isJsonArray()) {
            return gson.toJson(element);
        } else {
            return string;
        }
    }
}